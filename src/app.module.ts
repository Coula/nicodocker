import { Module } from '@nestjs/common';
import { CarsModule } from './cars/cars.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Car } from './cars/entities/car.entity';

@Module({
  imports: [TypeOrmModule.forRoot({
    type: 'mariadb',
    host: 'mariadb',
    port: 3306,
    username: 'root',
    password: 'qwerty',
    database: 'cours',
    entities: [Car],
    synchronize: true
  }), CarsModule],
})
export class AppModule {}
