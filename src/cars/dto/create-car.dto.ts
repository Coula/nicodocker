import { IsString,IsNotEmpty } from "class-validator";

export class CreateCarDto {
    @IsString()
    @IsNotEmpty()
    brand: string;

    model: string;

    year: number;

    plate: string;
}
